#Little Snitch Ad Block Rules

| File | Description  | Suscribe |
|:--|:--|:--|
| AdBlock.lsrules | General rules for booking ads and tracking. | [Click to suscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fbitbucket.org%2Fzxero%2Flittlesnitchadblockingrules%2Fraw%2FHEAD%2FAdBlock.lsrules) |
| Facebook.lsrules | Rules for blocking Facebook ads and tracking.  | [Click to suscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fbitbucket.org%2Fzxero%2Flittlesnitchadblockingrules%2Fraw%2FHEAD%2FFacebook.lsrules) |
| Google.lsrules | Rules for blocking Google ads and tracking.  | [Click to suscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fbitbucket.org%2Fzxero%2Flittlesnitchadblockingrules%2Fraw%2FHEAD%2FGoogle.lsrules) |